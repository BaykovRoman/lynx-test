import React from 'react';
import {Box, Container, Grid} from "@mui/material";
import {Routes, Route, Link} from "react-router-dom";
import Login from "./views/Login";
import ForgotPassword from "./views/ForgotPassword";
import Signup from "./views/Signup";
import Navigation from "./components/Navigation";

function App() {
    return (
        <Container maxWidth="md">
            <Box p={2}>
                <Routes>
                    <Route path="/" element={<Login/>}/>
                    <Route path="forgot" element={<ForgotPassword/>}/>
                    <Route path="signup" element={<Signup/>}/>
                </Routes>
            </Box>
            <Grid
                container
                spacing={0}
                direction="row"
                alignItems="center"
                justifyContent="center"
                // style={{ minHeight: '100vh' }}
            >
                <Navigation/>
            </Grid>
        </Container>
    );
}

export default App;
