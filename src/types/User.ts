export type User = {
    email: string,
    passwordHash: string,
}