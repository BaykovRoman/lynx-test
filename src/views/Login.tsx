import React, {ReactElement, useState} from 'react';
import {useSelector, useDispatch} from "react-redux";
import {login, logout} from "../store/reducers/authSlice";
import {Box, Button, Grid, TextField, Typography} from "@mui/material";
import {authUser} from "../store/sessionStore";
import {RootState} from "../store/store";
import {User} from "../types/User";

const Login = (): ReactElement => {

    const dispatch = useDispatch();

    const loggedInUser = useSelector((state: RootState) => state.auth.loggedInUser) as User;

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [errorMsg, setErrorMsg] = useState('');

    const signIn = (): void => {
        setErrorMsg("");
        const user = authUser(email, password);
        if (user) {
            dispatch(login(user));
        } else {
            setErrorMsg("Wrong email or password");
        }
    }

    if (!loggedInUser) {
        return (
            <Grid
                container
                spacing={0}
                direction="row"
                alignItems="center"
                justifyContent="center"
                // style={{ minHeight: '100vh' }}
            >

                <Grid item xs={12} my={1}>
                    <TextField
                        fullWidth label="Email" variant="outlined"
                        value={email}
                        onChange={(event): void => setEmail(event.target.value)}
                    />
                </Grid>
                <Grid item xs={12} my={1}>
                    <TextField
                        fullWidth
                        type="password"
                        label="Password"
                        variant="outlined"
                        value={password}
                        onChange={(event): void => setPassword(event.target.value)}
                    />
                </Grid>

                <Grid item xs={12} my={1}>
                    <Box sx={{display: 'flex', justifyContent: 'center'}} mx="auto">
                        <Button
                            variant="contained"
                            onClick={(event): void => signIn()}>
                            Login
                        </Button>
                    </Box>
                </Grid>

                <Grid item xs={12} my={1}>
                    <Box sx={{display: 'flex', justifyContent: 'center'}} mx="auto">
                        <Typography>
                            {errorMsg}
                        </Typography>
                    </Box>
                </Grid>

            </Grid>
        );
    } else {
        return (
            <Grid
                container
                spacing={0}
                direction="row"
                alignItems="center"
                justifyContent="center"
                // style={{ minHeight: '100vh' }}
            >
                <Grid item xs={12} my={1}>
                    <Box sx={{display: 'flex', justifyContent: 'center'}} mx="auto">
                        <Typography>
                            {`Hello ${loggedInUser.email}!`}
                        </Typography>
                    </Box>
                </Grid>

                <Grid item xs={12} my={1}>
                    <Box sx={{display: 'flex', justifyContent: 'center'}} mx="auto">
                        <Button
                            variant="contained"
                            onClick={(event): void => {dispatch(logout());}}>
                            Logout
                        </Button>
                    </Box>
                </Grid>
            </Grid>
        );
    }
}

export default Login;
