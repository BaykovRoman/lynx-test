import React, {ReactElement, useState} from 'react';
import {Box, Button, Grid, TextField, Typography} from "@mui/material";
import {getUserByEmail} from "../store/sessionStore";

const ForgotPassword = (): ReactElement => {

    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');

    const recoverPassword = (): void => {
        setMessage('');
        if(getUserByEmail(email)){
            setMessage("OTP has been sent");
        }else{
            setMessage("User not found");
        }
    }

    return (
        <Grid
            container
            spacing={0}
            direction="row"
            alignItems="center"
            justifyContent="center"
            // style={{ minHeight: '100vh' }}
        >

            <Grid item xs={12} my={1}>
                <TextField
                    fullWidth label="Email" variant="outlined"
                    value={email}
                    onChange={(event): void => setEmail(event.target.value)}
                />
            </Grid>

            <Grid item xs={12} my={1}>
                <Box sx={{display: 'flex', justifyContent: 'center'}} mx="auto">
                    <Button
                        variant="contained"
                        onClick={(event): void => recoverPassword()}>
                        Recover
                    </Button>
                </Box>
            </Grid>

            <Grid item xs={12} my={1}>
                <Box sx={{display: 'flex', justifyContent: 'center'}} mx="auto">
                    <Typography>
                        {message}
                    </Typography>
                </Box>
            </Grid>

        </Grid>
    );
}

export default ForgotPassword;
