import React, {ReactElement, useState} from 'react';
import {Box, Button, Grid, TextField, Typography} from "@mui/material";
import {User} from "../types/User";
import {addUser} from "../store/sessionStore";

const md5 = require('md5');

const Signup = (): ReactElement => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordRepeat, setPasswordRepeat] = useState('');
    const [message, setMessage] = useState('');

    const validateEmail = (emailAddress: string) => {
        let exp = /\S+@\S+\.\S+/;
        return exp.test(emailAddress);
    }

    const validatePassword = (password: string, passwordRepeat: string) => {
        return password === passwordRepeat;
    }

    const register = () => {
        setMessage('');
        let user: User = {
            email: email,
            passwordHash: md5(password),
        };

        addUser(user, setMessage);
    }

    return (
        <Grid
            container
            spacing={0}
            direction="row"
            alignItems="center"
            justifyContent="center"
            // style={{ minHeight: '100vh' }}
        >

            <Grid item xs={12} my={1}>
                <TextField
                    error={!validateEmail(email)}
                    helperText={validateEmail(email) ? "" : "Incorrect email"}
                    fullWidth label="Email" variant="outlined"
                    value={email}
                    onChange={(event): void => setEmail(event.target.value)}
                />
            </Grid>
            <Grid item xs={12} my={1}>
                <TextField
                    error={!validatePassword(password, passwordRepeat)}
                    helperText={validatePassword(password, passwordRepeat) ? "" : "Passwords doesnt match"}
                    fullWidth
                    type="password"
                    label="Password"
                    variant="outlined"
                    value={password}
                    onChange={(event): void => setPassword(event.target.value)}
                />
            </Grid>
            <Grid item xs={12} my={1}>
                <TextField
                    error={!validatePassword(password, passwordRepeat)}
                    helperText={validatePassword(password, passwordRepeat) ? "" : "Passwords doesnt match"}
                    fullWidth
                    type="password"
                    label="Repeat password"
                    variant="outlined"
                    value={passwordRepeat}
                    onChange={(event): void => setPasswordRepeat(event.target.value)}
                />
            </Grid>

            <Grid item xs={12} my={1}>
                <Box sx={{display: 'flex', justifyContent: 'center'}} mx="auto">
                    <Button
                        disabled={!validatePassword(password, passwordRepeat) || !validateEmail(email)}
                        variant="contained"
                        onClick={(event): void => register()}>
                        Register
                    </Button>
                </Box>
            </Grid>

            <Grid item xs={12} my={1}>
                <Box sx={{display: 'flex', justifyContent: 'center'}} mx="auto">
                    <Typography>
                        {message}
                    </Typography>
                </Box>
            </Grid>

        </Grid>
    );
}

export default Signup;
