import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {User} from "../../types/User";

export interface authState {
    loggedInUser: User | undefined,
}

const initialState: authState = {
    loggedInUser: undefined,
}

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        login: (state, action: PayloadAction<User>) => {
           state.loggedInUser = action.payload;
        },
        logout: (state, action: PayloadAction<void>) => {
            state.loggedInUser = undefined;
        },
    },
});

export const {login, logout} = authSlice.actions;

export default authSlice.reducer;
