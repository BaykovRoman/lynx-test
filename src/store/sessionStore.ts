import {User} from "../types/User";
const md5 = require('md5');

export const addUser = (user: User, callback: (message: string) => void): void => {
    let storedUsers: Array<User> | null = JSON.parse(window.sessionStorage.getItem('users') as string);
    if (!storedUsers) {
        storedUsers = [];
    }

    const isRegistered = storedUsers.find((currentUser: User) => {
        return currentUser.email === user.email
    });

    if (isRegistered) {
        callback('User already registered');
    } else {
        storedUsers.push(user)
        window.sessionStorage.setItem('users', JSON.stringify(storedUsers));
        callback('Registration successfull!');
    }
}

export const authUser = (email: string, password: string): User | undefined => {
    let storedUsers: Array<User> | null = JSON.parse(window.sessionStorage.getItem('users') as string);
    if (!storedUsers) {
        storedUsers = [];
    }

    const user = storedUsers.find((currentUser: User) => {
        return currentUser.email === email && currentUser.passwordHash === md5(password);
    });

    return user;
}

export const getUserByEmail = (email: string): User | undefined => {
    let storedUsers: Array<User> | null = JSON.parse(window.sessionStorage.getItem('users') as string);
    if (!storedUsers) {
        storedUsers = [];
    }

    const user = storedUsers.find((currentUser: User) => {
        return currentUser.email === email;
    });

    return user;
}